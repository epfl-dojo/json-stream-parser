#!/usr/bin/ruby
FILENAME=ARGV[0]

special_chars = ['[', ']', '{', '}', ',', ':', '"', '\\']


def tokenize(fn)
  File.open(fn, 'r') do |io|
    while (char = io.sysread(1)) do
      if char != '"'
        yield char
      else
        quoted_string = char
        while char = io.sysread(1) do
          if char == '\\'
            quoted_string << char << io.sysread(1)
          elsif char == '"'
            quoted_string << char
            yield quoted_string
            break
          else
            quoted_string << char
          end
        end
      end
    end
  end
end

def debug (wat)
  # puts wat
end

def parse(f)
  depth = 0
  opening_tokens = ['[', '{']
  closing_tokens = [']', '}']
  toks = ""
  seen_first_bracket = false

  tokenize(f) do |tok|
    if ! seen_first_bracket
      if tok == '['
        seen_first_bracket = true
      end
      next
    end
    if ! (tok == "," && depth == 0)
      toks << tok
    end
    debug toks
    if opening_tokens.include? tok
      depth = depth + 1
    end
    if closing_tokens.include? tok
      depth = depth - 1
      if depth == 0
        yield toks
        toks = ""
      end
    end
    debug depth
  end
end

parse FILENAME do |t|
  puts t
end
